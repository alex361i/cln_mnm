/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mbs;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.application.Application; 
import javafx.scene.Group; 
import javafx.scene.Scene; 
import javafx.scene.paint.Color; 

import javafx.event.ActionEvent; 
import javafx.event.EventHandler; 

import javafx.stage.Stage; 
import javafx.stage.Modality;

import javafx.geometry.Insets; 
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.geometry.HPos;
import static javafx.geometry.HPos.RIGHT;

import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.paint.Color;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.paint.Color;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextArea;
import javafx.scene.control.MenuBar;
import javafx.scene.control.Menu; 
import javafx.scene.control.MenuItem; 
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.CheckMenuItemBuilder;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.RadioMenuItemBuilder;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;

import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCombination;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


    
import java.io.*;
import java.util.Scanner;
import java.util.*;
import java.util.Date;

/**
 *
 * @author yo
 */
public class FXMLTableViewController {
    @FXML private TableView<Person> tableView;
    @FXML private TextField firstNameField;
    @FXML private TextField lastNameField;
    @FXML private TextField emailField;
    
    @FXML
    protected void addPerson(ActionEvent event) {
        ObservableList<Person> data = tableView.getItems();
        data.add(new Person(firstNameField.getText(),
            lastNameField.getText(),
            emailField.getText()
        ));
        
        firstNameField.setText("");
        lastNameField.setText("");
        emailField.setText("");   
    }
}
   /* @FXML
    protected void addTransfer(ActionEvent event) {
        ObservableList<ReportTransfer> data = tableView.getItems();
        data.add(new ReportTransfer(firstNameField.getText(),
            lastNameField.getText(),
            emailField.getText()
        ));
        
        firstNameField.setText("");
        lastNameField.setText("");
        emailField.setText("");   
    }
   }*/
/*
        buttonTransferMOney.setOnAction(new EventHandler<ActionEvent>() { 
        @Override public void handle(ActionEvent event) { 
        
        
        Stage depositStage = new Stage();
        depositStage.setTitle("Transfer external");
        BorderPane Depositroot = new BorderPane();
        Scene scene = new Scene(Depositroot, 620, 550, Color.WHITE);
        
        // create a grid pane2
        GridPane gridpane2 = new GridPane();
        gridpane2.setPadding(new Insets(10));
        gridpane2.setHgap(10);
        gridpane2.setVgap(10);
        
         // create a grid pane3
        GridPane gridpane3 = new GridPane();
        gridpane3.setPadding(new Insets(10));
        gridpane3.setHgap(10);
        gridpane3.setVgap(10);
        
        // candidates label
        Label candidatesLbl = new Label("Choose Sender");
        GridPane.setHalignment(candidatesLbl, HPos.CENTER);
        gridpane3.add(candidatesLbl, 0, 0);
        
        Label accountTypeLbl = new Label("Send FROM sender's ");
        GridPane.setHalignment(accountTypeLbl, HPos.LEFT);
        gridpane2.add(accountTypeLbl, 0, 0);
        
        Label recipientLbl = new Label("Send TO recipient's");
        GridPane.setHalignment(recipientLbl, HPos.LEFT);
        gridpane2.add(recipientLbl, 0, 1);
        
        Label MoneyForDepositLbl = new Label("Sum for transfer");
        GridPane.setHalignment(MoneyForDepositLbl, HPos.LEFT);
        gridpane2.add(MoneyForDepositLbl, 0, 2);
        
        TextField transferMoneyFld = new TextField(); 
        GridPane.setHalignment(transferMoneyFld, HPos.CENTER);
        gridpane2.add(transferMoneyFld, 1, 2);
        
        Button makeDepositum = new Button("Make Transfer");
        gridpane2.add(makeDepositum, 0, 4);
        
         Text SenderErrorText = new Text("");
         Font serif = Font.font("Serif", 15); 
         SenderErrorText.setFont(serif); 
         SenderErrorText.setFill(Color.RED);
         
         Text RecipientErrorText = new Text("");
         RecipientErrorText.setFont(serif); 
         RecipientErrorText.setFill(Color.RED);
         
         Text moneyErrorText = new Text("");
         moneyErrorText.setFont(serif); 
         moneyErrorText.setFill(Color.RED);
         
         Text moneyErrorText2 = new Text("");
         moneyErrorText2.setFont(serif); 
         moneyErrorText2.setFill(Color.RED);
         
         Text validErrorText = new Text("");
         validErrorText.setFont(serif); 
         validErrorText.setFill(Color.RED);

         
         VBox vbox77 = new VBox(5);
         vbox77.getChildren().addAll(validErrorText,SenderErrorText,RecipientErrorText,moneyErrorText,moneyErrorText2);
         

          
        Label senderLbl = new Label("Sender");
        gridpane3.add(senderLbl, 2, 0);
        GridPane.setHalignment(senderLbl, HPos.CENTER);
        
        Label recipient2Lbl = new Label("Recipient");
        gridpane3.add(recipient2Lbl, 3, 0);
        GridPane.setHalignment(recipient2Lbl, HPos.CENTER);
        
        // Customers
        
        final ObservableList<String>candidates = FXCollections.observableArrayList(Nordea.getNames());
        final ListView<String>candidatesListView = new ListView<String>(candidates);
        candidatesListView.setPrefWidth(150);
        candidatesListView.setPrefHeight(200);

        gridpane3.add(candidatesListView, 0, 1);
        
        // Sender
        final ObservableList<String>senders = FXCollections.observableArrayList();
        final ListView<String>senderListView = new ListView<String>(senders);
        senderListView.setPrefWidth(150);
        senderListView.setPrefHeight(100);

        gridpane3.add(senderListView, 2, 1);
        
        // Recipient
        final ObservableList<String> recipients = FXCollections.observableArrayList();
        final ListView<String> recipientListView = new ListView<String>(recipients);
        recipientListView.setPrefWidth(150);
        recipientListView.setPrefHeight(100);

        gridpane3.add(recipientListView, 3, 1);

        
        // select SENDER
        Button chooseSender = new Button("Choose sender");
        chooseSender.setMinWidth(113.0);
        chooseSender.setPrefWidth(113.0);
        chooseSender.setMaxWidth(113.0);
        chooseSender.setOnAction(new EventHandler<ActionEvent>() {
        
         public void handle(ActionEvent event) {
                String potential = candidatesListView.getSelectionModel().getSelectedItem();
                if (potential != null && senders.size()<1) {
                    candidatesListView.getSelectionModel().clearSelection();
                    //candidates.remove(potential);
                    senders.add(potential);
                }
            }
        });
        
        // select Recipient
        Button chooseRecipient = new Button("Choose recipient");
        chooseRecipient.setMinWidth(113.0);
        chooseRecipient.setPrefWidth(113.0);
        chooseRecipient.setMaxWidth(113.0);
        chooseRecipient.setOnAction(new EventHandler<ActionEvent>() {

            public void handle(ActionEvent event) {
                String potential = candidatesListView.getSelectionModel().getSelectedItem();
                if (potential != null && recipients.size()<1) {
                    candidatesListView.getSelectionModel().clearSelection();
                    candidates.remove(potential);
                    recipients.add(potential);
                }
            }
        });
        
        // deselect customer
        Button deselectButton = new Button("Deselect");
        deselectButton.setMinWidth(113.0);
        deselectButton.setPrefWidth(113.0);
        deselectButton.setMaxWidth(113.0);
        
        deselectButton.setOnAction(new EventHandler<ActionEvent>() {
        
            public void handle(ActionEvent event) {
            
                String notSender = senderListView.getSelectionModel().getSelectedItem();
                if (notSender != null) 
                {
                    senderListView.getSelectionModel().clearSelection();
                    senders.remove(notSender);
                    //candidates.add(notSender);
                }
                
                 String notRecipient = recipientListView.getSelectionModel().getSelectedItem();
                if (notRecipient != null) 
                {
                    recipientListView.getSelectionModel().clearSelection();
                    recipients.remove(notRecipient);
                    candidates.add(notRecipient);
                }
            }
        });
        
       
        ChoiceBox cb = new ChoiceBox(FXCollections.observableArrayList(
        "Current account", "Savings account"));
        gridpane2.add(cb, 1, 0);
        cb.getSelectionModel().select(0);
        
        ChoiceBox cb2 = new ChoiceBox(FXCollections.observableArrayList(
        "Current account", "Savings account"));
        gridpane2.add(cb2, 1, 1);
        cb2.getSelectionModel().select(0);
        
        VBox vbox = new VBox(5);
        vbox.getChildren().addAll(chooseSender,chooseRecipient,deselectButton);
        
        gridpane3.add(vbox, 1, 1);
        GridPane.setConstraints(vbox, 1, 1, 1, 2,HPos.CENTER, VPos.CENTER);
        
        
        }

 }*/