/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc2.dataClasses;

/**
 *
 * @author yo
 */
public class Employee {
    

    
    private String id;
    private String cnp;
    private String eName;
    private String eAddress;
    private String ePhone;
    private String eEmail;

    public Employee() {
    }

    public Employee(String id, String cnp, String eName, String eAddress, String ePhone, String eEmail) {
        this.id = id;
        this.cnp = cnp;
        this.eName = eName;
        this.eAddress = eAddress;
        this.ePhone = ePhone;
        this.eEmail = eEmail;
         
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
         this.id = id;
       
    }
    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public String geteName() {
        return eName;
    }

    public void seteName(String eName) {
        this.eName = eName;
    }

    public String geteAddress() {
        return eAddress;
    }

    public void seteAddress(String eAddress) {
        this.eAddress = eAddress;
    }

    public String getePhone() {
        return ePhone;
    }

    public void setePhone(String ePhone) {
        this.ePhone = ePhone;
    }

    public String geteEmail() {
        return eEmail;
    }

    public void seteEmail(String eEmail) {
        this.eEmail = eEmail;
    }
    
    public String toString(){
        return id+" "+cnp+" "+eName+" "+eAddress+" "+ePhone+" "+eEmail;
    
}
}
