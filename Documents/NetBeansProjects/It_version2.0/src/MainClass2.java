
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author yo
 */
public class MainClass2 {
   public static void main(String[] args) throws Exception {
    ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream("clients.ser"));

    AccountReccordSerializable record;

    record = new AccountReccordSerializable(1, "firstName", "lastName", 0.1);
    output.writeObject(record);

    ObjectInputStream input = new ObjectInputStream(new FileInputStream("clients.ser"));
    record = (AccountReccordSerializable) input.readObject();

    System.out.printf("%-10d%-12s%-12s%10.2f\n", record.getAccount(), record.getFirstName(), record
        .getLastName(), record.getBalance());

    output.close();
  }
}
    

