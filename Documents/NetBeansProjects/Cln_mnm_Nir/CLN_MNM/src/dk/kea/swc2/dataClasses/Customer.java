/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc2.dataClasses;

/**
 *
 * @author yo
 */
public class Customer {
    
    private String cId;
    private String cvr;
    private String cName;
    private String cAddress1;
    private String cAddress2;
    private String cAddress3;
    private String cAddress4;
    private String cPhone;
    private String cEmail;

    public Customer() {
    }

    public Customer(String cId,String cvr, String cName, String cAddress1,String cAddress2,
            String cAddress3,String cAddress4, String cPhone, String cEmail) {
        this.cId=cId;
        this.cvr = cvr;
        this.cName = cName;
        this.cAddress1 = cAddress1;
        this.cAddress1 = cAddress2;
        this.cAddress1 = cAddress3;
        this.cAddress1 = cAddress4;
        this.cPhone = cPhone;
        this.cEmail = cEmail;
    }

    public String getcId() {
        return cId;
    }

    public void setcId(String cId) {
        this.cId = cId;
    }

    public String getCvr() {
        return cvr;
    }

    public void setCvr(String cvr) {
        this.cvr = cvr;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public String getcAddress1() {
        return cAddress1;
    }

    public void setcAddress1(String cAddress1) {
        this.cAddress1 = cAddress1;
    }

    public String getcAddress2() {
        return cAddress2;
    }

    public void setcAddress2(String cAddress2) {
        this.cAddress2 = cAddress2;
    }

    public String getcAddress3() {
        return cAddress3;
    }

    public void setcAddress3(String cAddress3) {
        this.cAddress3 = cAddress3;
    }

    public String getcAddress4() {
        return cAddress4;
    }

    public void setcAddress4(String cAddress4) {
        this.cAddress4 = cAddress4;
    }

    public String getcPhone() {
        return cPhone;
    }

    public void setcPhone(String cPhone) {
        this.cPhone = cPhone;
    }

    public String getcEmail() {
        return cEmail;
    }

    public void setcEmail(String cEmail) {
        this.cEmail = cEmail;
    }

}