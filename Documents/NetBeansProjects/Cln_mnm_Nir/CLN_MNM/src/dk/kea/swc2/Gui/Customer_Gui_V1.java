/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc2.Gui;

import com.sun.istack.internal.logging.Logger;
import dk.kea.swc2.dataClasses.ConnectToMySQL;
import dk.kea.swc2.dataClasses.Customer;
import dk.kea.swc2.dataClasses.Employee;
import dk.kea.swc2.dataClasses.DBConnector;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.swing.DefaultListModel;
import javax.swing.ListModel;
import javax.swing.UIManager;

/**
 *
 * @author yo
 */
public class Customer_Gui_V1 extends javax.swing.JFrame {

     private final     ArrayList<Employee>  employeeList = new ArrayList<>();
                       ArrayList<Customer>   customerList = new ArrayList<>();
     int ok=0;
     int k=0;
    
     public Customer_Gui_V1() {
      
         try {
         for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
             if ("Nimbus".equals(info.getName())) {
                 UIManager.setLookAndFeel(info.getClassName());
                 break;
        }
         }
         }
          catch (Exception e) {
    // If Nimbus is not available, you can set the GUI to another look and feel.
}
         
         initComponents();
         readData();
    }
     
     private ListModel <Employee> getListFromAlist(List<Employee>aEmp){
           DefaultListModel<Employee> result = new DefaultListModel<>();

        aEmp.stream().forEach((imp) -> {
            result.addElement(imp);
        });
        return result;
     } 
     private ListModel <Customer> getListFromlist(List<Customer>aCust){
           DefaultListModel<Customer> cResult = new DefaultListModel<>();

        aCust.stream().forEach((cust) -> {
            cResult.addElement(cust);
        });
        return cResult;
     }
public  void select()throws SQLException,ClassNotFoundException{
    
    Connection conn=DBConnector.getConnection();
        
        String sqlRead="SELECT * FROM employee;";
        
        
        Statement stmt=conn.createStatement();
        stmt.execute(sqlRead);
        ResultSet rs=stmt.getResultSet();
      
        int i=0;
        
        while(rs.next()){
            
            Employee  emp=new Employee();
            emp.setId(rs.getString("id"));
            emp.setCnp(rs.getString("cnp"));
            emp.seteName(rs.getString("eName"));
            emp.seteAddress(rs.getString("eAddress"));
            emp.setePhone(rs.getString("ePhone"));
            emp.seteEmail(rs.getString("eEmail"));
            employeeList.add(emp);
          
          empList.setModel(getListFromAlist(employeeList));  
        }
        
}
public  void cSelect()throws SQLException,ClassNotFoundException{
    
    Connection conn=DBConnector.getConnection();
        
        String sqlRead="SELECT * FROM customer;";
        
        
        Statement stmt=conn.createStatement();
        stmt.execute(sqlRead);
        ResultSet rs=stmt.getResultSet();
      
        int i=0;
        
        while(rs.next()){
            
            Customer  cust=new Customer();
            cust.setcId(rs.getString("cId"));
            cust.setCvr(rs.getString("cvr"));
            cust.setcName(rs.getString("cName"));
            cust.setcAddress1(rs.getString("cAddress1"));
            cust.setcAddress2(rs.getString("cAddress2"));
            cust.setcAddress3(rs.getString("cAddress3"));
            cust.setcAddress4(rs.getString("cAddress4"));
            cust.setcPhone(rs.getString("cPhone"));
            cust.setcEmail(rs.getString("cEmail"));
            customerList.add(cust);
          
          customerList.setModel(getListFromlist(customerList));  
        }
        
}
public void insertEmp(Employee employee)throws ClassNotFoundException{

        try {

            Connection conn = DBConnector.getConnection();

            

            String sql = "INSERT INTO employee (id,cnp,eName,eAddress,ePhone,eEmail) VALUES (?,?,?,?,?,?);";
          
            
               
                PreparedStatement pstmt = conn.prepareStatement(sql);
                //issue.getSubject();
                pstmt.setString(1, employee.getId());
                pstmt.setString(2, employee.getCnp());
                pstmt.setString(3, employee.geteName());
                pstmt.setString(4, employee.geteAddress());
                pstmt.setString(5, employee.getePhone());
                pstmt.setString(6, employee.geteEmail());
                pstmt.execute();
             

        } catch (SQLException ex) {

            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);

        }

       System.out.println("------------End-----------");

    }
public void insertCust(Customer customer)throws ClassNotFoundException{

        try {

            Connection conn = DBConnector.getConnection();

            

            String sql = "INSERT INTO customer (cId,cvr,cName,cAddress1,cAddress2,cAddress3,cAddress4,cPhone,cEmail) VALUES (?,?,?,?,?,?,?,?,?);";
          
            
               
                PreparedStatement pstmt = conn.prepareStatement(sql);
                //issue.getSubject();
                pstmt.setString(1, customer.getcId());
                pstmt.setString(2, customer.getCvr());
                pstmt.setString(3, customer.getcName());
                pstmt.setString(4, customer.getcAddress1());
                pstmt.setString(4, customer.getcAddress2());
                pstmt.setString(4, customer.getcAddress3());
                pstmt.setString(4, customer.getcAddress4());
                pstmt.setString(5, customer.getcPhone());
                pstmt.setString(6, customer.getcEmail());
                pstmt.execute();
             

        } catch (SQLException ex) {

            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);

        }

       System.out.println("------------End-----------");

    }

     public static void deleteEmp(Employee employee)throws ClassNotFoundException{
         
         try{
             Connection conn = DBConnector.getConnection();
             
             String sql = "DELETE FROM employee WHERE id = ?;";
             
             PreparedStatement pstmt = conn.prepareStatement(sql);
             
             pstmt.setString(1, employee.getId());
             
             pstmt.execute();
             
         }
         catch (SQLException ex){
             
         }
         
     }
      public static void deleteCust(Customer customer)throws ClassNotFoundException{
         
         try{
             Connection conn = DBConnector.getConnection();
             
             String sql = "DELETE FROM customer WHERE cId = ?;";
             
             PreparedStatement pstmt = conn.prepareStatement(sql);
             
             pstmt.setString(1, customer.getcId());
             
             pstmt.execute();
             
         }
         catch (SQLException ex){
             
         }
         
     }
     public  void updateEmp(Employee employee)throws ClassNotFoundException{
           
         try{
             Connection conn = DBConnector.getConnection();
             
             String sql = "UPDATE employee SET id=?, cnp=?,eName=?,eAddress=?,ePhone=?,eEmail=?;";
             
               
              PreparedStatement pstmt = conn.prepareStatement(sql);
              
               pstmt.setString(1, empIdTfield.getText()); 
               pstmt.setString(2, cprTxtField.getText());
               pstmt.setString(3,empNameTxtField.getText());
               pstmt.setString(4,empAdrtxttField.getText());
               pstmt.setString(5,empPhnTxtField.getText());
               pstmt.setString(6,empEmailTxtField.getText());
               
               pstmt.execute();
              
     }
          catch (SQLException ex){
         
      java.util.logging.Logger.getLogger(ConnectToMySQL.class.getName()).log(Level.SEVERE, null, ex);
     }
     System.out.println("--------------End---------------");
     }
     public  void updateCust(Customer Customer)throws ClassNotFoundException{
           
         try{
             Connection conn = DBConnector.getConnection();
             
             String sql = "UPDATE customer SET cId=?, cvr=?,cName=?,cAddress1=?"
                     + "cAddress2=?,cAddress3=?,cAddress4=?,cPhone=?,cEmail=?;";
             
               
              PreparedStatement pstmt = conn.prepareStatement(sql);
              
               pstmt.setString(1, cIdTextField.getText()); 
               pstmt.setString(2, cvrTxtField.getText());
               pstmt.setString(3,customerNameTField.getText());
               pstmt.setString(4,cAddress1TField.getText());
               pstmt.setString(5,cAddress2TField.getText());
               pstmt.setString(6,cAddress3TField.getText());
               pstmt.setString(7,cAddress4TField.getText());
               pstmt.setString(8,cPhoneTField.getText());
               pstmt.setString(9,cEmailTField.getText());
               
               pstmt.execute();
              
     }
          catch (SQLException ex){
         
      java.util.logging.Logger.getLogger(ConnectToMySQL.class.getName()).log(Level.SEVERE, null, ex);
     }
     System.out.println("--------------End---------------");
     }
     private void readData(){
        if (ok != 0) {
            DefaultListModel listModel = (DefaultListModel) empList.getModel();
            listModel.removeAllElements();
        }
        try {
            select();

        } catch (SQLException ex) {
            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        ok = 1;

                              
    }
  //private void readData(){} //to ask about to logs
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane3 = new javax.swing.JTabbedPane();
        jTabbedPane4 = new javax.swing.JTabbedPane();
        jMenu3 = new javax.swing.JMenu();
        jMenu6 = new javax.swing.JMenu();
        jMenu7 = new javax.swing.JMenu();
        jMenu8 = new javax.swing.JMenu();
        tabBar = new javax.swing.JTabbedPane();
        employeesTab = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        empList = new javax.swing.JList();
        cprTxtField = new javax.swing.JTextField();
        empAdrtxttField = new javax.swing.JTextField();
        empNameTxtField = new javax.swing.JTextField();
        empPhnTxtField = new javax.swing.JTextField();
        cprLabel = new javax.swing.JLabel();
        empNameLabel = new javax.swing.JLabel();
        empAdrLabel = new javax.swing.JLabel();
        empPhnLabel = new javax.swing.JLabel();
        empUpdateButton = new javax.swing.JButton();
        empDelButton = new javax.swing.JButton();
        empAddButton = new javax.swing.JButton();
        empEmailTxtField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        empIdLabel = new javax.swing.JLabel();
        empIdTfield = new javax.swing.JTextField();
        scheduleTab = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        schEmpComb = new javax.swing.JComboBox();
        schCustComb = new javax.swing.JComboBox();
        schDateFromComb = new javax.swing.JComboBox();
        shcWrkHouTxtField = new javax.swing.JTextField();
        schDateToComb = new javax.swing.JComboBox();
        empSchLabel = new javax.swing.JLabel();
        schCustLabel = new javax.swing.JLabel();
        shcWrkHouLabel = new javax.swing.JLabel();
        schDateFrLabel = new javax.swing.JLabel();
        schDateToLabel = new javax.swing.JLabel();
        schConfButton = new javax.swing.JButton();
        schGnReport = new javax.swing.JButton();
        schUpDateButt = new javax.swing.JButton();
        schDeleteButt = new javax.swing.JButton();
        dateEffective = new javax.swing.JLabel();
        dateWhen = new javax.swing.JComboBox();
        Location = new javax.swing.JComboBox();
        schLoctLabel = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        schEmp_CstLabel = new javax.swing.JLabel();
        schReportLabel = new javax.swing.JLabel();
        schSchduleLabel = new javax.swing.JLabel();
        customerTab = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        custemerList = new javax.swing.JList();
        cvr = new javax.swing.JLabel();
        cvrTxtField = new javax.swing.JTextField();
        customerName = new javax.swing.JLabel();
        customerNameTField = new javax.swing.JTextField();
        CaddressMLabel = new javax.swing.JLabel();
        cAddress1TField = new javax.swing.JTextField();
        phoneLabel = new javax.swing.JLabel();
        cPhoneTField = new javax.swing.JTextField();
        emailLabel = new javax.swing.JLabel();
        cEmailTField = new javax.swing.JTextField();
        custAddButton = new javax.swing.JButton();
        updateCButton = new javax.swing.JButton();
        deleteCbutton = new javax.swing.JButton();
        cAddress3TField = new javax.swing.JTextField();
        cAddress2TField = new javax.swing.JTextField();
        cAddress4TField = new javax.swing.JTextField();
        Caddress2Label = new javax.swing.JLabel();
        Caddress3Label = new javax.swing.JLabel();
        Caddress4Label = new javax.swing.JLabel();
        cIdTextField = new javax.swing.JTextField();
        cIdLabel = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();

        jTabbedPane3.addTab("tab1", jTabbedPane4);

        jMenu3.setText("jMenu3");

        jMenu6.setText("jMenu6");

        jMenu7.setText("jMenu7");

        jMenu8.setText("jMenu8");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        empList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        empList.setToolTipText("");
        empList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                empListValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(empList);

        empAdrtxttField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                empAdrtxttFieldActionPerformed(evt);
            }
        });

        cprLabel.setText("CPR");

        empNameLabel.setText("Name");

        empAdrLabel.setText("Address");

        empPhnLabel.setText("Phone");

        empUpdateButton.setText("Update");
        empUpdateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                empUpdateButtonActionPerformed(evt);
            }
        });

        empDelButton.setText("Delete");
        empDelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                empDelButtonActionPerformed(evt);
            }
        });

        empAddButton.setText("Add");
        empAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                empAddButtonActionPerformed(evt);
            }
        });

        jLabel1.setText("Email");

        empIdLabel.setText("ID");

        empIdTfield.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                empIdTfieldActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout employeesTabLayout = new javax.swing.GroupLayout(employeesTab);
        employeesTab.setLayout(employeesTabLayout);
        employeesTabLayout.setHorizontalGroup(
            employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(employeesTabLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(employeesTabLayout.createSequentialGroup()
                        .addGroup(employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(empAdrLabel)
                            .addComponent(jLabel1)
                            .addComponent(empPhnLabel)
                            .addComponent(empNameLabel)
                            .addComponent(cprLabel))
                        .addGap(25, 25, 25)
                        .addGroup(employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(empAddButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(empNameTxtField, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
                            .addComponent(cprTxtField)
                            .addComponent(empAdrtxttField)
                            .addComponent(empPhnTxtField, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
                            .addComponent(empEmailTxtField))
                        .addGroup(employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(employeesTabLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(empUpdateButton, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(empDelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(employeesTabLayout.createSequentialGroup()
                                .addGap(61, 61, 61)
                                .addComponent(empIdLabel)
                                .addGap(18, 18, 18)
                                .addComponent(empIdTfield, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 151, Short.MAX_VALUE)))
                .addContainerGap())
        );
        employeesTabLayout.setVerticalGroup(
            employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(employeesTabLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cprTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cprLabel)
                    .addComponent(empIdLabel)
                    .addComponent(empIdTfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(empNameLabel)
                    .addComponent(empNameTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(empAdrtxttField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(empAdrLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(empPhnLabel)
                    .addComponent(empPhnTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(empEmailTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(empAddButton)
                    .addComponent(empUpdateButton)
                    .addComponent(empDelButton))
                .addGap(0, 42, Short.MAX_VALUE))
        );

        tabBar.addTab("Employees", employeesTab);

        jList1.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane3.setViewportView(jList1);

        schEmpComb.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "name 1", "Item 2", "Item 3", "Item 4" }));

        schCustComb.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "customer 1", "Item 2", "Item 3", "Item 4" }));

        schDateFromComb.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "dd/mm/yy", "Item 2", "Item 3", "Item 4" }));
        schDateFromComb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                schDateFromCombActionPerformed(evt);
            }
        });

        schDateToComb.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "dd/mm/yy", "Item 2", "Item 3", "Item 4" }));

        empSchLabel.setText("Set Employe");

        schCustLabel.setText("Customer");

        shcWrkHouLabel.setText("Working Hours");

        schDateFrLabel.setText("Date From");

        schDateToLabel.setText("Date To");

        schConfButton.setText("O.K");

        schGnReport.setText("Generate Report");

        schUpDateButt.setText("Update");

        schDeleteButt.setText("Delete");

        dateEffective.setText("Date");

        dateWhen.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "dd/mm/yy", "Item 2", "Item 3", "Item 4" }));
        dateWhen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dateWhenActionPerformed(evt);
            }
        });

        Location.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        schLoctLabel.setText("Location");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        schEmp_CstLabel.setText(" Employee/Customer Name");

        schReportLabel.setText("REPORTS");

        schSchduleLabel.setText("SCHEDULE");

        javax.swing.GroupLayout scheduleTabLayout = new javax.swing.GroupLayout(scheduleTab);
        scheduleTab.setLayout(scheduleTabLayout);
        scheduleTabLayout.setHorizontalGroup(
            scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(scheduleTabLayout.createSequentialGroup()
                .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(scheduleTabLayout.createSequentialGroup()
                        .addComponent(schEmp_CstLabel)
                        .addGap(42, 42, 42)
                        .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(scheduleTabLayout.createSequentialGroup()
                                .addComponent(schDateFrLabel)
                                .addGap(13, 13, 13)
                                .addComponent(schDateFromComb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(scheduleTabLayout.createSequentialGroup()
                                .addComponent(schDateToLabel)
                                .addGap(18, 18, 18)
                                .addComponent(schDateToComb, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(scheduleTabLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(empSchLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(schEmpComb, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(45, 45, 45)
                        .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(schCustLabel)
                            .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(schCustComb, 0, 105, Short.MAX_VALUE)
                                .addComponent(dateWhen, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(18, 18, 18)
                        .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Location, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(schLoctLabel)
                            .addComponent(schConfButton, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 252, Short.MAX_VALUE)
                .addComponent(schUpDateButt))
            .addGroup(scheduleTabLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(scheduleTabLayout.createSequentialGroup()
                        .addComponent(schReportLabel)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(scheduleTabLayout.createSequentialGroup()
                        .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(schSchduleLabel)
                            .addGroup(scheduleTabLayout.createSequentialGroup()
                                .addComponent(shcWrkHouLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(shcWrkHouTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(dateEffective)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(schDeleteButt, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, scheduleTabLayout.createSequentialGroup()
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(schGnReport, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        scheduleTabLayout.setVerticalGroup(
            scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(scheduleTabLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(schSchduleLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(empSchLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(schCustLabel)
                    .addComponent(schLoctLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(schEmpComb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(schCustComb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Location, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(schUpDateButt))
                .addGap(2, 2, 2)
                .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dateEffective)
                    .addComponent(dateWhen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(shcWrkHouLabel)
                    .addComponent(schDeleteButt)
                    .addComponent(shcWrkHouTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(schConfButton))
                .addGap(22, 22, 22)
                .addComponent(schReportLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(schDateFromComb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(schDateFrLabel)
                    .addComponent(schEmp_CstLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(schDateToComb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(schDateToLabel)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(schGnReport))
                .addGap(23, 23, 23))
        );

        tabBar.addTab("Schedule", scheduleTab);

        custemerList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(custemerList);

        cvr.setText("CVR");

        customerName.setText("C.Name");

        CaddressMLabel.setText("Main Address");

        cAddress1TField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cAddress1TFieldActionPerformed(evt);
            }
        });

        phoneLabel.setText("Phone");

        emailLabel.setText("C.Email");

        custAddButton.setText("Add");
        custAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                custAddButtonActionPerformed(evt);
            }
        });

        updateCButton.setText("Update");

        deleteCbutton.setText("Delete");

        cAddress4TField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cAddress4TFieldActionPerformed(evt);
            }
        });

        Caddress2Label.setText("Address 2");

        Caddress3Label.setText("Address 3");

        Caddress4Label.setText("Address 4");

        cIdLabel.setText("C.Id");

        javax.swing.GroupLayout customerTabLayout = new javax.swing.GroupLayout(customerTab);
        customerTab.setLayout(customerTabLayout);
        customerTabLayout.setHorizontalGroup(
            customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(customerTabLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, customerTabLayout.createSequentialGroup()
                        .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(customerTabLayout.createSequentialGroup()
                                .addComponent(cIdLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cIdTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(117, 117, 117))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, customerTabLayout.createSequentialGroup()
                                .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cvr)
                                    .addComponent(emailLabel)
                                    .addComponent(phoneLabel)
                                    .addComponent(customerName))
                                .addGap(22, 22, 22)
                                .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cEmailTField, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cvrTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cPhoneTField, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(customerNameTField, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(customerTabLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(custAddButton, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(8, 8, 8)
                                .addComponent(updateCButton, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(deleteCbutton, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(7, 7, 7))
                            .addGroup(customerTabLayout.createSequentialGroup()
                                .addGap(64, 64, 64)
                                .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(CaddressMLabel)
                                    .addComponent(Caddress2Label)
                                    .addComponent(Caddress3Label)
                                    .addComponent(Caddress4Label))
                                .addGap(18, 18, 18)
                                .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cAddress4TField, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                                    .addComponent(cAddress1TField)
                                    .addComponent(cAddress2TField)
                                    .addComponent(cAddress3TField))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(customerTabLayout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addContainerGap())))
        );
        customerTabLayout.setVerticalGroup(
            customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(customerTabLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CaddressMLabel)
                    .addComponent(cAddress1TField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cIdTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cIdLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cAddress2TField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(Caddress2Label)
                        .addComponent(cvrTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(cvr, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cAddress3TField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Caddress3Label)
                    .addComponent(customerNameTField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(customerName))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cAddress4TField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Caddress4Label)
                    .addComponent(cPhoneTField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(phoneLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cEmailTField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(emailLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
                .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(custAddButton)
                    .addComponent(updateCButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(deleteCbutton))
                .addContainerGap(13, Short.MAX_VALUE))
        );

        tabBar.addTab("Customers", customerTab);

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabBar)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabBar)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cAddress1TFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cAddress1TFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cAddress1TFieldActionPerformed

    private void schDateFromCombActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_schDateFromCombActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_schDateFromCombActionPerformed

    private void dateWhenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dateWhenActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_dateWhenActionPerformed

    private void empAdrtxttFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_empAdrtxttFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_empAdrtxttFieldActionPerformed

    private void empUpdateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_empUpdateButtonActionPerformed
          
        int selectedIndex = empList.getSelectedIndex();
   
    String id = empIdTfield.getText();
    String cnp = cprTxtField.getText();
    String eName = empNameTxtField.getText();
    String eAddress =empAdrtxttField.getText();
    String ePhone = empPhnTxtField.getText();
    String eEmail = empEmailTxtField.getText();
    
     Employee em = new Employee(id, cnp, eName, eAddress, ePhone, eEmail);// really need a constractor?
     
     try{
         updateEmp(em);
         System.out.println("item updated");
     }
     catch (ClassNotFoundException ex){
     java.util.logging.Logger.getLogger(ConnectToMySQL.class.getName()).log(Level.SEVERE, null, ex);
      employeeList.remove(selectedIndex);
      employeeList.add(selectedIndex, em);
      
      empList.setModel(getListFromAlist(employeeList));

     }
       
    }//GEN-LAST:event_empUpdateButtonActionPerformed

    private void empDelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_empDelButtonActionPerformed
       
         int selectedIndex = empList.getSelectedIndex();
         
          Employee empl = (Employee) empList.getSelectedValue();
          
          if (empl!=null){
              employeeList.remove(selectedIndex);
              
              empList.setModel(getListFromAlist( employeeList));
              
              try{
                  deleteEmp(empl);
              }
              catch (ClassNotFoundException ex){
                  java.util.logging.Logger.getLogger(ConnectToMySQL.class.getName()).log(Level.SEVERE, null, ex);
              }
          }
        
        
    }//GEN-LAST:event_empDelButtonActionPerformed

    private void empListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_empListValueChanged
        // TODO add your handling code here:
        
        Employee empl = (Employee) empList.getSelectedValue();
        if (empl != null) {
            
            empIdTfield.setText(empl.getId());
            cprTxtField.setText(empl.getCnp());
            empNameTxtField.setText(empl.geteName());
            empAdrtxttField.setText(empl.geteAddress());
            empPhnTxtField.setText(empl.getePhone());
            empEmailTxtField.setText(empl.geteEmail());
        }


    }//GEN-LAST:event_empListValueChanged

    private void empAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_empAddButtonActionPerformed
        // TODO add your handling code here:
    String id = empIdTfield.getText();
    String cnp = cprTxtField.getText();
    String eName = empNameTxtField.getText();
    String eAddress =empAdrtxttField.getText();
    String ePhone = empPhnTxtField.getText();
    String eEmail = empEmailTxtField.getText();
    
    Employee em = new Employee(id ,cnp, eName, eAddress, ePhone, eEmail);// really need a constractor?
   
    employeeList.add(em);
    empList.setModel(getListFromAlist(employeeList));
   try {
                    insertEmp(em);
                } catch (ClassNotFoundException ex) {
                    java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);
                }
    }//GEN-LAST:event_empAddButtonActionPerformed

    private void cAddress4TFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cAddress4TFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cAddress4TFieldActionPerformed

    private void empIdTfieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_empIdTfieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_empIdTfieldActionPerformed

    private void custAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_custAddButtonActionPerformed
/*
    String cId = cIdTextField.getText();
    String cvr = cvrTxtField.getText();
    String cName = customerNameTField.getText();
    String cAddress1 =cAddress1TField.getText();
    String cAddress2 =cAddress2TField.getText();
    String cAddress3 =cAddress3TField.getText();
    String cAddress4 =cAddress4TField.getText();
    String cPhone =cPhoneTField.getText();
    String cEmail = cEmailTField.getText();
    
    Customer cus = new Customer(cId ,cvr, cName, cAddress1,cAddress2,cAddress3,cAddress4, cPhone, cEmail);
   
    customerList.add(cus);
    aCust.setModel(getListFromAlist(customerList));
   try {
                    insertEmp(em);
                } catch (ClassNotFoundException ex) {
                    java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);
                }
    }                         
*/

// TODO add your handling code here:
    }//GEN-LAST:event_custAddButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Customer_Gui_V1().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Caddress2Label;
    private javax.swing.JLabel Caddress3Label;
    private javax.swing.JLabel Caddress4Label;
    private javax.swing.JLabel CaddressMLabel;
    private javax.swing.JComboBox Location;
    private javax.swing.JTextField cAddress1TField;
    private javax.swing.JTextField cAddress2TField;
    private javax.swing.JTextField cAddress3TField;
    private javax.swing.JTextField cAddress4TField;
    private javax.swing.JTextField cEmailTField;
    private javax.swing.JLabel cIdLabel;
    private javax.swing.JTextField cIdTextField;
    private javax.swing.JTextField cPhoneTField;
    private javax.swing.JLabel cprLabel;
    private javax.swing.JTextField cprTxtField;
    private javax.swing.JButton custAddButton;
    private javax.swing.JList custemerList;
    private javax.swing.JLabel customerName;
    private javax.swing.JTextField customerNameTField;
    private javax.swing.JPanel customerTab;
    private javax.swing.JLabel cvr;
    private javax.swing.JTextField cvrTxtField;
    private javax.swing.JLabel dateEffective;
    private javax.swing.JComboBox dateWhen;
    private javax.swing.JButton deleteCbutton;
    private javax.swing.JLabel emailLabel;
    private javax.swing.JButton empAddButton;
    private javax.swing.JLabel empAdrLabel;
    private javax.swing.JTextField empAdrtxttField;
    private javax.swing.JButton empDelButton;
    private javax.swing.JTextField empEmailTxtField;
    private javax.swing.JLabel empIdLabel;
    private javax.swing.JTextField empIdTfield;
    private javax.swing.JList empList;
    private javax.swing.JLabel empNameLabel;
    private javax.swing.JTextField empNameTxtField;
    private javax.swing.JLabel empPhnLabel;
    private javax.swing.JTextField empPhnTxtField;
    private javax.swing.JLabel empSchLabel;
    private javax.swing.JButton empUpdateButton;
    private javax.swing.JPanel employeesTab;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JList jList1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenu jMenu8;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane3;
    private javax.swing.JTabbedPane jTabbedPane4;
    private javax.swing.JLabel phoneLabel;
    private javax.swing.JButton schConfButton;
    private javax.swing.JComboBox schCustComb;
    private javax.swing.JLabel schCustLabel;
    private javax.swing.JLabel schDateFrLabel;
    private javax.swing.JComboBox schDateFromComb;
    private javax.swing.JComboBox schDateToComb;
    private javax.swing.JLabel schDateToLabel;
    private javax.swing.JButton schDeleteButt;
    private javax.swing.JComboBox schEmpComb;
    private javax.swing.JLabel schEmp_CstLabel;
    private javax.swing.JButton schGnReport;
    private javax.swing.JLabel schLoctLabel;
    private javax.swing.JLabel schReportLabel;
    private javax.swing.JLabel schSchduleLabel;
    private javax.swing.JButton schUpDateButt;
    private javax.swing.JPanel scheduleTab;
    private javax.swing.JLabel shcWrkHouLabel;
    private javax.swing.JTextField shcWrkHouTxtField;
    private javax.swing.JTabbedPane tabBar;
    private javax.swing.JButton updateCButton;
    // End of variables declaration//GEN-END:variables
}
