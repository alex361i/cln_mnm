package dk.kea.swc2.dataClasses;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alex
 */
public class DBConnector {

    private static final String USER = "root"; //the name of the mysql user
    private static final String PASS = "";     // the pass of the mysql user
    private static final String DB = "cln_mnm"; // the name of your DB
    private static final String URL = "jdbc:mysql://localhost"; // the url for your DB server
    private static final String PORT = "3306"; // the port your DB server runs on

    public static Connection getConnection() {
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            //jdbc:mysql://localhost:3306/firstdb
            String urlForConn = URL + ":" + PORT + "/" + DB;
            conn = DriverManager.getConnection(urlForConn, USER, PASS);
        } catch (Exception ex) {
            Logger.getLogger(DBConnector.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("[ERR] "+ex.getMessage());
        } 
        return conn;
    }
}
