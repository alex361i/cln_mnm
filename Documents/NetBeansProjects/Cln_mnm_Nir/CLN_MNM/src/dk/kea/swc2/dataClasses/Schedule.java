/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc2.dataClasses;

/**
 *
 * @author yo
 */
public class Schedule {
    private String hours;
    private String sDate;
    private String eDate;

    public Schedule() {
    }

    public Schedule(String hours, String sDate, String eDate) {
        this.hours = hours;
        this.sDate = sDate;
        this.eDate = eDate;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getsDate() {
        return sDate;
    }

    public void setsDate(String sDate) {
        this.sDate = sDate;
    }

    public String geteDate() {
        return eDate;
    }

    public void seteDate(String eDate) {
        this.eDate = eDate;
    }
    
    
}
