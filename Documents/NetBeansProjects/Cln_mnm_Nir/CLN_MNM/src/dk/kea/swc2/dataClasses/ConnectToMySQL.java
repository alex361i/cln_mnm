package dk.kea.swc2.dataClasses;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alex
 */
public class ConnectToMySQL {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try {
            Connection conn = DBConnector.getConnection();

            String sql = "SELECT * FROM employee;";

            Statement stmt = conn.createStatement();
            stmt.execute(sql);

           ResultSet rs = stmt.getResultSet();
            while (rs.next()) {
                int id = rs.getInt("id");
                int cpr = rs.getInt("cnp");
                String Name = rs.getString("eName");
                String Address = rs.getString("eAddress");
                String Phone = rs.getString("ePhone");
                String Email = rs.getString("eEmail");
                System.out.println("id = "+ id + " cnp = " + cpr + " eName = " + Name + " eAddress = " + Address +"ePhone = " + Phone +"eEmail = " + Email);
            }

        } catch (SQLException ex) {
         //   Logger.getLogger(ConnectToMySQL.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
