/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mbs;

/**
 *
 * @author yo
 */
//this account (savingAcc) has an interestRate
 public class SavingsAcc extends BankAcc
 {  
    
    public SavingsAcc(double rate) 
    {  
       interestRate = rate;
   }
 
    
    public void addInterest() 
    {  
       double interest = getBalance() * interestRate / 100;
       deposit(interest); 
    }

    private double interestRate;
 }
