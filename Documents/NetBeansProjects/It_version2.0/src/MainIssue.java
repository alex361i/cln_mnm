
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author yo
 */
public class MainIssue implements Serializable{
    public static void main(String[] args) throws Exception{
        ObjectOutputStream output=new ObjectOutputStream(new FileOutputStream("issue.ser"));
        
        Issue record;
        record= new Issue("technical",1,1,"nu imi merge creierul");
        output.writeObject(record);
        
        ObjectInputStream input= new ObjectInputStream(new FileInputStream("issue.ser"));
        record=(Issue) input.readObject();
        System.out.println(record.getSubject()+" "+record.getPriority()+" "+record.getStatus()+" "+record.getComment());
        output.close();
        
    }
    
}
