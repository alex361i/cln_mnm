/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc2.dataClasses;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author NIr
 */
public class ConnectToMySQLcustomer {
     public static void main(String[] args) {

        try {
            Connection conn = DBConnector.getConnection();

            String sql = "SELECT * FROM customer;";

            Statement stmt = conn.createStatement();
            stmt.execute(sql);

           ResultSet rs = stmt.getResultSet();
            while (rs.next()) {
                int cId = rs.getInt("cId");
                int cvr = rs.getInt("cvr");
                String cName = rs.getString("cName");
                String cAddress1 = rs.getString("cAddress1");
                String cAddress2 = rs.getString("cAddress2");
                String cAddress3 = rs.getString("cAddress3");
                String cAddress4 = rs.getString("cAddress4");
                String Phone = rs.getString("ePhone");
                String Email = rs.getString("eEmail");
                System.out.println("cId = "+ cId + " cvp = " + cvr + " cName = " + cName + 
                        " cAddress1 = " + cAddress1 +" cAddress2 = " + cAddress2 +" cAddress3 = " + cAddress3 +
                        " cAddress4 = " + cAddress4 +"ePhone = " + Phone +"eEmail = " + Email);
            }

        } catch (SQLException ex) {
         //   Logger.getLogger(ConnectToMySQL.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}

    
